model = require './ff_model';
natural = require 'natural'
_ = require 'underscore';

model.fetch 'subscriptions', 'posts' , ->
    _.each model.data.subscriptions, (sub) ->
        classifier = new natural.BayesClassifier()
        _.each model.data.posts, (post) ->
            if post.feedId == sub.id
                classifier.addDocument(post.title + ' ' + post.summary, post.category || '2 neutral');

        classifier.train()
        model.set('subscriptions.'+sub.id+'.classifier', JSON.stringify(classifier))

        console.log 'Updated', sub.url, sub.id

    process.exit()
