model = require './ff_model';
rssparser = require 'rssparser';
natural = require 'natural'
_ = require 'underscore';

feeds_count = 0

model.subscribe 'subscriptions', 'posts', ->
    _.each model.data.subscriptions, (sub) ->
        restoredClassifier = sub.classifier && natural.BayesClassifier.restore JSON.parse(sub.classifier)
        allow_classifying = restoredClassifier && restoredClassifier.classifier.totalExamples > 10
        feeds_count += 1
        console.log('Updating', sub.url);
        rssparser.parseURL sub.url, {}, (err, feed_data) ->
            cnt = 0
            _.each feed_data.items, (post) ->
                post_url = post.url || post.guid.link
                existing_post = _.findWhere model.data.posts, {url: post_url, userId: sub.userId}
                if !existing_post
                    cat = allow_classifying && restoredClassifier.classify(post.title + ' ' + post.summary)
                    update_data = {
                        url: post_url,
                        feedId: sub.id,
                        userId: sub.userId,
                        title: post.title,
                        summary: post.summary,
                        category: cat,
                        published_at: post.published_at
                    }
                    model.add 'posts', update_data
                    cnt += 1
            console.log 'Updated', sub.url, cnt

            feeds_count -= 1

            ff_exit = () ->
                process.exit()

            if feeds_count == 0
                setTimeout(ff_exit, 1000)

# {"is_read": {"$ne": true}}