model = require './ff_model';
natural = require 'natural'
_ = require 'underscore';

model.fetch 'subscriptions', 'posts', ->
    _.each model.data.subscriptions, (sub) ->
        restoredClassifier = sub.classifier && natural.BayesClassifier.restore JSON.parse(sub.classifier)
        if restoredClassifier && restoredClassifier.classifier.totalExamples > 5
            console.log('Classifying', sub.url);
            cnt = 0
            _.each model.data.posts, (post) ->
                if post.feedId == sub.id && !post.category
                    cnt += 1
                    model.set('posts.' + post.id + '.category', restoredClassifier.classify(post.title + ' ' + post.summary),)
            console.log 'Updated', cnt

    process.exit()