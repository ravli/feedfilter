express = require 'express'
derby = require 'derby'
racerBrowserChannel = require 'racer-browserchannel'
liveDbMongo = require 'livedb-mongo'
coffeeify = require 'coffeeify'
MongoStore = require('connect-mongo')(express)
rssparser = require 'rssparser';
natural = require 'natural'
_ = require 'underscore';

app = require '../app/index.coffee'
error = require './error.coffee'

expressApp = module.exports = express()

# Get Redis configuration
if process.env.REDIS_HOST
    redis = require('redis').createClient process.env.REDIS_PORT, process.env.REDIS_HOST
    redis.auth process.env.REDIS_PASSWORD
else if process.env.REDISCLOUD_URL
    redisUrl = require('url').parse process.env.REDISCLOUD_URL
    redis = require('redis').createClient redisUrl.port, redisUrl.hostname
    redis.auth redisUrl.auth.split(":")[1]
else
    redis = require('redis').createClient()
redis.select process.env.REDIS_DB || 1
# Get Mongo configuration 
mongoUrl = process.env.MONGO_URL || process.env.MONGOHQ_URL ||
'mongodb://localhost:27017/project'

# The store creates models and syncs data
store = derby.createStore
    db:
        db: liveDbMongo(mongoUrl + '?auto_reconnect', safe: true)
        redis: redis

store.on 'bundle', (browserify) ->
    # Add support for directly requiring coffeescript in browserify bundles
    browserify.transform coffeeify

createUserId = (req, res, next) ->
    model = req.getModel()
    console.log 'request session', req.session
    userId = req.session.userId
    if userId
        model.set '_session.userId', userId
    next()

model = store.createModel()

auth = require("derby-passport")
strategies =
    facebook:
        strategy: require("passport-facebook").Strategy
        conf:
            clientID: '616721128364710'
            clientSecret: '1c1937d434d3e1e2363eadc305cc0279'
            callbackURL: "http://feedfilter.ilvar.ru/auth/facebook/callback"
    twitter:
        strategy: require("passport-twitter").Strategy
        conf:
            consumerKey: 'NNliOE4Q6kCQNIX5oLL94Q'
            consumerSecret: 'HXq1aRZZ6xbm3yHUsKSlNJ1UWs7hCHpKtz0rQzFeyo'
            callbackURL: "http://feedfilter.ilvar.ru/auth/twitter/callback"

passport_options =
    passport:
        failureRedirect: '/'
        successRedirect: '/'
    site:
        domain: 'http://feedfilter.ilvar.ru'
        name: 'Feed Filter'
        email: 'ilvar@mail.ru'

mongoskin = require('mongoskin')
ms_db = mongoskin.db(mongoUrl + '?auto_reconnect', {safe: true})
auth.store(store, ms_db, strategies)

expressApp
# Gzip dynamically
.use(express.compress())
# Respond to requests for application script bundles
.use(app.scripts store)
# Serve static files from the public directory
.use(express.static __dirname + '/../../public')

# Add browserchannel client-side scripts to model bundles created by store,
# and return middleware for responding to remote client messages
.use(racerBrowserChannel store)
# Add req.getModel() method
.use(store.modelMiddleware())

# Parse form data
# .use(express.bodyParser())
# .use(express.methodOverride())

# Session middleware
.use(express.cookieParser())
.use(express.session({
        secret: process.env.SESSION_SECRET || 'YOUR SECRET HERE',
        store: new MongoStore({url: mongoUrl + '?auto_reconnect', safe: true, auto_reconnect: true})
    })
)
.use(auth.middleware(strategies, passport_options))
#.use(createUserId)

# Create an express middleware from the app's routes
.use(app.router())
.use(expressApp.router)
.use(error())

# SERVER-SIDE ROUTES #

expressApp.get '/auth/logout', (req, res) ->
    res.clearCookie 'connect.sid'
    res.redirect '/'

expressApp.get '/update', (req, res) ->
    model = req.getModel()
    model.fetch 'users', 'subscriptions', 'posts', ->
        all_posts = _.values(model.data.posts)
        _.each model.data.users, (user, userId) ->
            user.id = user.id || userId
            _.each model.data.subscriptions, (sub, subId) ->
                sub.id = sub.id || subId
                if sub.userId != user.id
                    return

                allow_classifying = false
                restoredClassifier = null
                if !allow_classifying && sub.classifier
                    restoredClassifier = natural.BayesClassifier.restore JSON.parse(sub.classifier)
                    allow_classifying = restoredClassifier && restoredClassifier.classifier.totalExamples > 10

                if !allow_classifying && user.classifier
                    restoredClassifier = natural.BayesClassifier.restore JSON.parse(user.classifier)
                    allow_classifying = restoredClassifier && restoredClassifier.classifier.totalExamples > 10

                console.log('Updating', sub.url);
                rssparser.parseURL sub.url, {}, (err, feed_data) ->
                    cnt = 0
                    _.each feed_data.items, (post) ->
                        post_url = post.url || post.guid.link
                        existing_post = _.findWhere all_posts, {url: post_url, feedId: sub.id}
                        if !existing_post
                            post_text = post.title + ' ' + post.summary

                            cat = null
                            classiffication_data = []
                            if allow_classifying
                                cat = restoredClassifier.classify(post_text)
                                classiffication_data = restoredClassifier.getClassifications(post_text)

                            classiffication_data = _.map classiffication_data, (cd) ->
                                return [cd.label.replace(/^\d\s/ig, ''), cd.value]

                            update_data = {
                                url: post_url,
                                feedId: sub.id,
                                userId: sub.userId,
                                title: post.title,
                                summary: post.summary,
                                category: cat,
                                category_data: _.object(classiffication_data),
                                published_at: post.published_at
                            }
                            model.add 'posts', update_data
                            cnt += 1
                    console.log 'Updated', sub.url, cnt

        model.unfetch 'users', 'subscriptions', 'posts'
        res.redirect '/'

expressApp.get '/train', (req, res) ->
    model = req.getModel()
    model.fetch 'users', 'subscriptions', 'posts', ->
        _.each model.data.users, (user, userId) ->
            user.id = user.id || userId
            classifier = new natural.BayesClassifier()
            _.each model.data.posts, (post) ->
                if post.userId == user.id
                    classifier.addDocument(post.title + ' ' + post.summary, post.category || '2 neutral');

            classifier.train()
            model.set('users.' + user.id + '.classifier', JSON.stringify(classifier))

        _.each model.data.subscriptions, (sub, subId) ->
            sub.id = sub.id || subId
            classifier = new natural.BayesClassifier()
            _.each model.data.posts, (post) ->
                if post.feedId == sub.id
                    classifier.addDocument(post.title + ' ' + post.summary, post.category || '2 neutral');

            classifier.train()
            model.set('subscriptions.' + sub.id + '.classifier', JSON.stringify(classifier))

        model.unfetch 'users', 'subscriptions', 'posts'
        res.redirect '/'

expressApp.all '*', (req, res, next) ->
    next '404: ' + req.url

