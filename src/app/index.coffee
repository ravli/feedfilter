app = require('derby').createApp(module)

# ROUTES #

render_posts_page = (page, model, template) ->
    userId = model.get '_session.userId'
    user = model.at 'users.' + userId
    subscriptionsQuery = model.query 'subscriptions', {userId: userId}
    postsQuery = model.query 'posts', {is_read: {$ne: true}, category: {$ne: "3 bad"}, userId: userId, $orderby: {"category_data.good": -1, category: -1}}

    model.subscribe user, subscriptionsQuery, postsQuery, (err) ->
        model.ref '_page.user', user
        subscriptionsQuery.ref '_page.subscriptions'
        postsQuery.ref '_page.posts'
        page.render template

app.get '/', (page, model) ->
    render_posts_page page, model, 'home'

app.get '/subscriptions', (page, model) ->
    render_posts_page page, model, 'subscriptions'

app.get '/feed', (page, model) ->
    render_posts_page page, model, 'feed'

# CONTROLLER FUNCTIONS #

app.fn 'subscriptions.add', (e, el) ->
    newSub = @model.del '_page.newSub'
    return unless newSub
    newSub.userId = @model.get '_session.userId'
    @model.add 'subscriptions', newSub

app.fn 'subscriptions.remove', (e, el, next) ->
    item = e.get ':sub'
    @model.del 'subscriptions.' + item.id

app.fn 'feed.mark_good', (e, el, next) ->
    item = e.get ':post'
    @model.set('posts.'+item.id+'.is_read', true)
    @model.set('posts.'+item.id+'.category', '1 good')

app.fn 'feed.mark_read', (e, el, next) ->
    item = e.get ':post'
    @model.set('posts.'+item.id+'.is_read', true)
    @model.set('posts.'+item.id+'.category', '2 neutral')

app.fn 'feed.mark_bad', (e, el, next) ->
    item = e.get ':post'
    @model.set('posts.'+item.id+'.is_read', true)
    @model.set('posts.'+item.id+'.category', '3 bad')

