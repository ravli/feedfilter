liveDbMongo = require 'livedb-mongo'

redis = null
if process.env.REDIS_HOST
  redis = require('redis').createClient process.env.REDIS_PORT, process.env.REDIS_HOST
  redis.auth process.env.REDIS_PASSWORD
else if process.env.REDISCLOUD_URL
  redisUrl = require('url').parse process.env.REDISCLOUD_URL
  redis = require('redis').createClient redisUrl.port, redisUrl.hostname
  redis.auth redisUrl.auth.split(":")[1]
else
  redis = require('redis').createClient()
redis.select process.env.REDIS_DB || 1

mongoUrl = process.env.MONGO_URL || process.env.MONGOHQ_URL ||'mongodb://localhost:27017/project'
db = liveDbMongo(mongoUrl + '?auto_reconnect', safe: true)
store = require('derby').createStore {db: db, redis: redis}

module.exports = store.createModel()
